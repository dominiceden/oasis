$(function(){

	// Fire a VPV when the user logs in during the checkout process
	$('.returning-customers > form').submit(function(){
		ga('send', 'pageview', '/checkout/vpv/login');
	});

	// Fire an event when the user checks the box to receive the newsletter when going through the guest checkout process.
	$('.login_checkout > form').submit(function(){
		if ( $('#dwfrm_login_signupnewsletter').is(':checked') == true ) {
			ga( 'set', { 'dimension8': 'true' } );
			sendGAEventIfReceiveNewsletterBoxChecked();
		}
		else {
			ga( 'set', { 'dimension8': 'false' } );
			sendGAEventIfReceiveNewsletterBoxChecked();
		}
		ga('send', 'pageview', '/checkout/vpv/guest');
	});

	// Define a function to send a GA event if the receive newsletter box is ticked.
	function sendGAEventIfReceiveNewsletterBoxChecked() {
		ga(
			'send',
			'event',
			'Newsletter signup',
			'Submit',
			'via registration'
		);
	}

	// Report the stage the user is at when they proceed through Checkout
	// Step 1: Delivery
	if ($('.checkout-progress-indicator .active span span').text() == 'Delivery') {

		ga('ec:setAction','checkout', {'step': 1});

		var checkoutURLParameters = digitalData.page.url.split('?').pop();
		// Check for parameters in the page URL to see if the user has logged in during checkout, is using Guest Checkout, or was logged in before checkout.
		// Drop session cookies so we can inspect the checkout method on the Order Confirmation page.

		if ( checkoutURLParameters.indexOf('analyticsLoginType=checkout%20login') >= 0 ) { // If user has logged in during checkout
			document.cookie = 'userCheckoutType=userLoggedInDuringCheckout;path=/';
			ga('send', 'pageview', '/shipping/vpv/login');
		}

		else if ( checkoutURLParameters.indexOf('analyticsLoginType=guest%20checkout') >= 0 ) { // If the user is using Guest Checkout
			document.cookie = 'userCheckoutType=userUsingGuestCheckout;path=/';
			ga('send', 'pageview', '/shipping/vpv/guest');
		}

		else { // If the user was already logged in before checkout, send a VPV.
			document.cookie = 'userCheckoutType=userLoggedInBeforeCheckout;path=/';
			ga('send', 'pageview', '/checkout/vpv/registered');
		}
	}

	// Step 2: Your Details (also known as the Billing page)
	if ($('.checkout-progress-indicator .active span span').text() == 'Your Details') {
		ga('ec:setAction','checkout', {'step': 2});

		if (determineCheckoutUserType() == 'userLoggedInDuringCheckout') {
			ga('send', 'pageview', '/billing/vpv/login');
		}

		else if (determineCheckoutUserType() == 'userUsingGuestCheckout') {
			ga('send', 'pageview', '/billing/vpv/guest');
		}

		else if (determineCheckoutUserType() == 'userLoggedInBeforeCheckout') {
			ga('send', 'pageview', '/billing/vpv/registered');
		}


		// Step 3: Payment
		// Attach a click handler to the button where the user clicks to go to the payment page.
		// This payment page is not on Demandware, so we cannot add code to it. The event must be bound to the button instead.
		$('button#toPayment').click(function(){
			ga('ec:setAction','checkout', {'step': 3});

			if (determineCheckoutUserType() == 'userLoggedInDuringCheckout') {
				ga('send', 'pageview', '/payment/vpv/login');
			}

			else if (determineCheckoutUserType() == 'userUsingGuestCheckout') {
				ga('send', 'pageview', '/payment/vpv/guest');
			}

			else if (determineCheckoutUserType() == 'userLoggedInBeforeCheckout') {
				ga('send', 'pageview', '/payment/vpv/registered');
			}

		});
	}

	// This function checks the cookies to see what the user's checkout type is, and returns the checkout type so appropriate GA events can then be run depending on the checkout type.
	function determineCheckoutUserType() {
		var splitCookieArray = document.cookie.split(';')
		for (var i = 0; i < splitCookieArray.length; i++) {
			if (splitCookieArray[i].indexOf('userCheckoutType') >= 0) {
				var userCheckoutType = splitCookieArray[i].split('=').pop(); // Get user checkout type (e.g. guest checkout, logged in before checkout or already logged in before checkout)
				console.log('User checkout type detected - checkout type is ' + userCheckoutType);
				return userCheckoutType;
				break;
			}
		}
	}

	if (window.secondaryGATracking == true) {

		// If on delivery page
		if ($('.checkout-progress-indicator .active span span').text() == 'Delivery') {

			// Track promo codes applied during checkout
			setInterval(function(){
				$('#minicartcoupons').unbind('submit');
				$('#minicartcoupons').submit(function(){
					var voucherCode = $('input.voucher-code').val().trim();

					if (voucherCode.length == 0) { // If the first field (the regular discount code) is empty, then check the unidays input field.
						voucherCode == $('input.voucher-code2').val().trim();
					}

					setTimeout(function(){ // Check that the promocode has been applied successfully (allow a delay for verification)
						if ( digitalData.bag.promocodes.length > 0 && digitalData.bag.promocodes.indexOf(voucherCode) >= 0 ) { // If the digitalData object contains the newly added promocode
							ga(
								'send',
								'event',
								'Checkout',
								'Apply coupon',
								voucherCode
							);
						}
					}, 2000);
				});
			}, 2000);

			// When the user proceeds to the next page (Billing), send their chosen delivery type.
			$('.button_primary').click(function(){
				var selectedDeliveryType = $('table#calendar td.selected a').attr('data-method-id') || '';
				if (selectedDeliveryType.length == 0) {
					selectedDeliveryType = 'Seek & Send'
				}
				ga(
					'send',
					'event',
					'Checkout',
					'Delivery options',
					selectedDeliveryType
				);
			});

			// If the select address button is present on the page (i.e. the user has a saved address)
			if ($('.selectaddressbtn').length > 0) {
				ga(
					'send',
					'event',
					'Checkout',
					'Saved delivery address',
					'Saved delivery address *'
				);
			}

		}

		// Send checkout form fields titles after the user has left the field (onblur) - don't add blur function to student code input field as we have a separate event for that below
		$('input,select','#primary, #secondary').blur(function() {
			var eventAction = $(this).parents('form').attr('id');
			var eventlabel = $('label[for="' + $(this).attr('name') + '"]');
			if (eventlabel.length != 0) {
				eventlabel = eventlabel.text();
			}
			else {
				eventlabel = $(this).attr('placeholder');
			}
			if ((typeof eventlabel === 'undefined') || eventlabel === false) {
				eventlabel = $(this).attr('id');
			}
			if ((typeof eventlabel != 'undefined') && eventlabel != false) {
				eventlabel = eventlabel.trim().replace(/^\s+|\s+$/g, '').replace(/(\r\n|\n|\r)/gm,'-');
			}

			ga(
				'send',
				'event',
				'Checkout',
				'Billing form',
				eventlabel
			);
		});


	} // End secondaryGATracking

});

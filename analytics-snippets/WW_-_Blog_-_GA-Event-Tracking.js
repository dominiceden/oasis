$(function(){

  if (window.location.href.indexOf('blog') > 0) {

    // Track clicks on social links
    $('#insta-widget').click(function(){
      ga(
        'send',
        'event',
        'Social',
        'blog',
        'Instagram'
      );
    });

    $('body').on('click', '#twit-widget', function(){
      ga(
        'send',
        'event',
        'Social',
        'blog',
        'Twitter'
      );
    });

  }

});

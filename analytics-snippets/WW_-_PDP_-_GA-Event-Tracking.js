$(function(){

  // Get the eight digit SKU and the product variant colour code, which is the last 2 digits of the 8 digit SKU.
  var eightDigitSku = $('.updatingPdpMainsku').text().trim();
  var productVariantCode = eightDigitSku.slice(-2);

  // Add to bag
  $('.product_details').on('click', '.action-addtocart', function() {
    setTimeout(function () {
      if ($('.select-product-size-message').length===0) {
        ga('ec:addProduct',{
          'id': eightDigitSku,
          'name': digitalData.page.product.name,
          'category': digitalData.page.product.masterCategory,
          'variant': productVariantCode
        });
        ga('ec:setAction', 'add');
        ga(
          'send',
          'event',
          'Product',
          'Move to bag',
          eightDigitSku
        );
      }
    }, 100);
  });

  // Add to wishlist
  $('body').on('click', '.product_details .save_for_later', function() {
    ga(
      'send',
      'event',
      'Add to wishlist - from item page',
      digitalData.page.product.masterCategory,
      eightDigitSku
    );

    // If the product being added to wishlist has at least one size that's low on stock, then send another GA event
    $('ul.size li').each(function(){
      if ( $(this).hasClass('low-stock') ) {
        ga(
          'send',
          'event',
          'Add to wishlist - low stock',
          digitalData.page.product.masterCategory,
          eightDigitSku
        );
        return false;
      }
    });
  });

  // Play video on PDP
  $('body').on('click', 'img.videothumbnail', function(){
    ga(
      'send',
      'event',
      'Product',
      'Video play',
      eightDigitSku
    );
  });


  if (window.secondaryGATracking == true) {

    // Write a review
    $('body').on('click', 'button.bv-write-review', function(){
      var refreshInterval = setInterval(function(){ // Start interval to check if the BazaarVoice lightbox is open
        // console.log('start interval');

        if ( $('.bv-mbox-opened').length > 0 ) { // If the box is open, attach click handlers.

          // console.log('bv box opened');
          $('.bv-rating-link').click(function(){
            // console.log('clicked star rating');
            clickedProductRating = $(this).attr('id').slice(-1);
            // console.log(clickedProductRating);
          });

          $('.bv-form-actions-submit').click(function(){
            ga(
              'send',
              'event',
              'Product',
              'Review',
              eightDigitSku + ' - ' + clickedProductRating
            );
          });

          clearInterval(refreshInterval); // Cancel the interval if the box is open.
          // console.log('cancel interval');
        }

      }, 200);
    });

    // Find in a store - Postcode
    $('body').on('click', '#check-store-availability', function(){
      var refreshInterval = setInterval(function(){ // Set an interval to check for the store availability dialog to appear
        if ( $('.stock_check_dialog').length > 0 ) {
          $('.store-locator-search.find-in-store').click(function(){
            var submittedPostcode = $('#dwfrm_storelocator_postalCode').val().toUpperCase().split(' ');
            for (var i = 0; i < submittedPostcode.length; i++) {
              if (submittedPostcode[i].match(/\d+/g) != null) { // If the postcode contains a number (e.g. is not a town name) then send the postcode.
                ga( 'set', { 'dimension4': submittedPostcode[i].substring(0,3) } );
                break;
              }
            }
            ga(
              'send',
              'event',
              'Product',
              'Find in store',
              eightDigitSku
            );
          });
          clearInterval(refreshInterval); // Cancel the interval once the dialog is open
        }
      }, 500);
    });

    // When the user clicks on the product info tabs (e.g. Description, Information, Size Guide)
    $('.product-info .toggle_container').click(function(){
      if ( !$(this).hasClass('expanded') ) { // If the info tab is not already open, send GA event on click.
        var label = $(this).find('h3.toggle_handle').text().trim();
        ga(
          'send',
          'event',
          'Product',
          'Extra information',
          label
        );
      }
    });

    // Click on item in Baynote Recently Viewed section
    $('#recently-viewed-baynote').on('click', 'a', function(e) {
      var eventLabel = $(this).parents('.product-tile').attr('data-itemid').substring(0,8);
      ga(
        'send',
        'event',
        'Recent view',
        'Click',
        eventLabel
      );
    });

    // Report size out of stock
    $(window).load(function(){
      var eightDigitSku = $('.updatingPdpMainsku').text().trim();
      var reportSizesOutOfStock = true;
      var sizesOutOfStock = eightDigitSku + ' - '; // String to store sizes that are OOS

      $('ul.size li').each(function(){
        if ( $(this).hasClass('out-of-stock') ) { // If size is OOS
          // Strip text from string so only the size number remains
          var outOfStockSize = $(this).find('a').text().trim() + ', ';
          sizesOutOfStock += outOfStockSize; // Add to string
        }
      });

      if (reportSizesOutOfStock == true) {
        sizesOutOfStock = sizesOutOfStock.substring(0, sizesOutOfStock.length - 2);
        ga(
          'send',
          'event',
          'Product',
          'Out of stock',
          sizesOutOfStock
        );
        reportSizesOutOfStock = false; // Set it to false to stop it being triggered again if the user clicks the menu again.
      }
    });

    // When the user interacts with the picture
    $('body').on('click', 'li.thumb, .product-primary-image-container', function(){
      ga(
        'send',
        'event',
        'Product',
        'Product image interaction',
        eightDigitSku
      );
    });

  } // End secondaryGATracking

});

for (var i = 0; i < digitalData.bag.products.length; i++) {
  var product = digitalData.bag.products[i];
  ga('ec:addProduct', {
    'id': product.id,
    'name': product.name,
    'category': product.masterCategory,
    'variant': product.colour,
    'price': product.price,
    'coupon': digitalData.bag.promocodes,
    'quantity': product.quantity
  });
}

ga('ec:setAction', 'purchase', {
  'id': digitalData.orderId,
  'revenue': digitalData.bag.totals.grandTotal,
  'tax': digitalData.bag.totals.tax,
  'shipping': digitalData.delivery.price,
  'coupon': digitalData.bag.promocodes
});

// Set the delivery option chosen as a custom dimension
ga( 'set', { 'dimension9': digitalData.delivery.id } );

if (determineCheckoutUserType() == 'userLoggedInDuringCheckout') {
  ga('send', 'pageview', '/orderconfirmation/vpv/login');
}

else if (determineCheckoutUserType() == 'userUsingGuestCheckout') {
  ga('send', 'pageview', '/orderconfirmation/vpv/guest');
}

else if (determineCheckoutUserType() == 'userLoggedInBeforeCheckout') {
  ga('send', 'pageview', '/orderconfirmation/vpv/registered');
}

// This function checks the cookies to see what the user's checkout type is, and returns the checkout type so appropriate GA events can then be run depending on the checkout type.
function determineCheckoutUserType() {
  var splitCookieArray = document.cookie.split(';')
  for (var i = 0; i < splitCookieArray.length; i++) {
    if (splitCookieArray[i].indexOf('userCheckoutType') >= 0) {
      var userCheckoutType = splitCookieArray[i].split('=').pop(); // Get user checkout type (e.g. guest checkout, logged in before checkout or already logged in before checkout)
      console.log('User checkout type detected - checkout type is ' + userCheckoutType);
      return userCheckoutType;
      break;
    }
  }
}

// This script fires GA events sitewide.

$(function(){

  // When a user searches, fire a virtual pageview if the search term redirects to a static page instead of a search results page.
  // Create an object containing the search rules for the site (in the format 'searchTerm': 'redirectURL')
  var searchRules = {
    "just in"                  : "/new-in",
    "new arrivals"             : "/new-in",
    "what's new"               : "/new-in",
    "about"                    : "/about-us",
    "about us"                 : "/about-us",
    "history"                  : "/about-us",
    "careers"                  : "/join-us/jobs-landing.html",
    "employment"               : "/join-us/jobs-landing.html",
    "jobs"                     : "/join-us/jobs-landing.html",
    "delivery"                 : "/shipping",
    "shipping"                 : "/shipping",
    "timetables"               : "/shipping",
    "account"                  : "/account",
    "my account"               : "/account",
    "order history"            : "/account",
    "store"                    : "/stores",
    "store locator"            : "/stores",
    "stores"                   : "/stores",
    "blog"                     : "/oasis-fashion.html",
    "in-the-know"              : "/oasis-fashion.html",
    "MPS"                      : "/oasis-fashion.html",
    "my personal stylist"      : "/oasis-fashion.html",
    "social"                   : "/oasis-fashion.html",
    "styling"                  : "/oasis-fashion.html",
    "sunday girl"              : "/oasis-fashion.html"
  }
  var searchRulesKeys = Object.keys(searchRules); // Create array of the keys for the searchRules object so we can loop through it.

  $('.header-search form').submit(function(){
    var searchTerm = $('input#q').val();
    var numberOfAutoSuggestedSearchResults = $('.search-suggestion-right-panel-product, .search-suggestion-right-panel-product-last').length.toString();
    for (var i = 0; i < searchRulesKeys.length; i++ ) {
      if (searchTerm == searchRulesKeys[i] ) { // If the search term equals a key in the redirect rules, send a GA virtual pageview.
        ga('send', 'event', 'Site search', 'Common terms', searchTerm);
      }
    }
  });

  // Fire a Virtual Pageview when a user clicks on an auto-suggested search result in the search bar.
  $('body').on('click', '.search-suggestion-right-panel-product', function(){
    var searchClickedProductName = $(this).find('a.search-suggestion-normal-link').text().trim();
    ga('send', 'event', 'Site search', 'Click on an item', searchClickedProductName);
  });

  $('body').on('click', '[data-track]', function (e) {
    var gaData = $.trim($(this).attr('data-track')).replace(/\s+/g, " ").split(',');
    if (gaData.length===3) {
      ga('send', 'event', gaData[0], gaData[1], gaData[2]);
        cmCreateElementTag( gaData[0], gaData[1], gaData[2] );
      }
  });

  // When the user clicks a social button in the footer
  $('.social-links li a').click(function(){
    ga('send', 'event', 'Social', 'Click', $(this).find('.link_label').text());
  });

  // Newsletter signup in the footer.
  $( '.newsletter-signup' ).on( 'submit', '#sign-up', function(){
  	ga('send', 'event', 'Newsletter sign up', 'Submit', 'via footer' );
  });

  if (window.secondaryGATracking == true) {

    // When a user clicks the Help link
    $('li[data-link="footer-help-link"], #header-help-link').click(function(){
      ga('send', 'event', 'Help', 'Click', window.location.href);
    });

    // Fire event when the Checkout link is clicked when hovering over the Bag link in the navigation
    $('body').on('click', '.mini-cart-link-checkout.checkout-button', function(){
      ga(
        'send',
        'event',
        'Bag - Nav',
        'Click',
        window.location.pathname
      );
    });

    // Track when user changes language by clicking the site selector in the footer
    $('a.siteSelect').click(function(){
      setTimeout(function(){
        $('#country-selector-form').submit(function(){
          var languageSelected = $('a.language_selector > span.selectBox-label').text();
          ga(
            'send',
            'event',
            'Language',
            'Click',
            languageSelected
          );
        });
      }, 1500);
    });

  } // End secondaryGATracking

});

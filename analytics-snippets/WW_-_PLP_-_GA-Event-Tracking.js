$(function(){

  // Fire a CoreMetrics Shop 5 tag when a product is added to cart on the PLP.
  $('body').on('click', '.action-addtocart', function(){
    var eightDigitSku = $('.updatingPdpMainsku').text().trim();
    var productName = $('h1.product_name').text().trim();
    var productPrice = $('#QuickViewDialog .price-sales').text().trim().slice(1);

    if ( $('#isSizeSelected').val() == 'true' ) {
      cmCreateShopAction5Tag(
        eightDigitSku,
        productName,
        "1",
        productPrice
      );
      cmDisplayShop5s();
    }
  });

  if (window.secondaryGATracking == true) {
    // When the user sets their display preferences at the top of the PLP
    $('body').on('click', '.top_filters', function(){
      var label = $(this).find('.active').text(); // Get the new label of the filter clicked on (i.e. what it has changed to)
      sendDisplayPreferencesEvent(label);
    });
    // When the user chooses to sort the list of products (e.g. by price high to low)
    // We have to unbind and rebind the change event as Demandware removes the event once the first change event has occurred.
    setInterval(function(){
      $('#grid-sort-header.sort_dropdown').unbind('change');
      $('#grid-sort-header.sort_dropdown').change(function(){
        var label = $(this).find(':selected').text(); // Get the new label of the filter clicked on (i.e. what it has changed to)
        sendDisplayPreferencesEvent(label);
      });
    }, 2000);
    function sendDisplayPreferencesEvent(label) {
      ga(
        'send',
        'event',
        'Product list',
        'Display preference',
        label
      );
    }
    // Adding and removing filters
    $('#main').on('click', '.filters_wrapper ul:not(".available_in_store") li a', function(e) {
      var eventAction = '';
      if ($(e.currentTarget).parent().hasClass('selected')) {
        eventAction = 'Remove filter';
      }
      else if ($(e.currentTarget).parents('.active_refinements').length > 0) {
        eventAction = 'Remove filter';
      }
      else {
        eventAction = 'Add filter';
      }
      var eventLabel = $(e.currentTarget).children('label').text().split('(')[0].trim();
      // If the eventLabel is undefined, it might be because the filter is in the header, not in the main list of filters.
      // In that case, set the correct eventLabel.
      if (eventLabel.length == 0) {
        eventLabel = $(e.currentTarget).prev().text().trim();
      }
      ga(
        'send',
        'event',
        'Product list',
        eventAction,
        eventLabel
      );
    });
    $(document).ready(function(){
      // When the user clicks on a PLP item
      $('.product-tile-grid').on('click', '.wl-action.save_for_later, .quickviewbutton, .flex-direction-nav', function(){
        var productImageURL = $( this ).parents( '.product-tile-grid' ).find( '.flex-active-slide .thumb-link > img' ).attr( 'src' );
        var eightDigitSku = productImageURL.split('/').pop().split('_')[1];
        var sixDigitSku = eightDigitSku.substring(0,6);
        var productName = $(".product-tile[data-itemid='" + sixDigitSku + "']").find('a.name-link').text().trim();
        // If they clicked on the Add To Wishlist button and the item has not already been saved
        if ( $(this).hasClass('save_for_later') && !$(this).hasClass('item_saved') ) {
          ga(
            'send',
            'event',
            'Product list',
            'Save for later',
            eightDigitSku
          );
        }
        // If they clicked on the Quick View button
        else if ( $(this).hasClass('quickviewbutton') ) {
          ga(
            'send',
            'event',
            'Product list',
            'Product details popup',
            eightDigitSku
          );
        }
        // If they clicked on the previous or next slideshow arrows
        else if ( $(this).hasClass('flex-direction-nav') ) {
          ga(
            'send',
            'event',
            'Product list',
            'Product image interaction',
            eightDigitSku
          );
        }
      });
    });
    // Report a VPV to GA when more products are loaded onto the screen from infinite scroll.
    $(window).load(function(){
      // Collect an initial number of products displayed on the page.
      var numberOfProductsShown = $('li.grid-tile').length;
      $(window).scroll(function(){
        if ($('li.grid-tile').length > numberOfProductsShown) {
          numberOfProductsShown = $('li.grid-tile').length;
          var currentPageNumber = window.location.search.split('=').pop() || digitalData.page.id.split('_').pop(); // Get current page number. If no page number set in URL, use the search term.
          ga(
            'send',
            'event',
            'Dynamic Content',
            'Load',
            window.location.pathname + window.location.search
          );
        }
      });
    });
    // When the user clicks on a nav item from the PLP
    $( '.header_navigation' ).on( 'click', 'li.subsubitems > ul > li > a', function(){
      ga(
        'send',
        'event',
        'Product list',
        'Click to other product list: ' + $(this).text().trim(),
        'Clicked from: ' + digitalData.page.category.name
      );
    });
    // When the user reaches the end of the PLP
    $(document).ready(function(){
      setTimeout(function(){
        var refreshInterval = setInterval(function() { // Start a timer that checks to see if the user is at the end of the page (minus footer and Recently Viewed sections)
          if ((window.innerHeight + window.scrollY) >= ($(document).height() - $('footer').height() - $('#recently-viewed-baynote').height() )) {
            clearInterval(refreshInterval); // Cancel the timer.
            ga(
              'send',
              'event',
              'Dynamic content',
              'Ends',
              window.location.pathname
            );
          }
        }, 200);
      }, 2000);
    });
  } // End secondaryGATracking
});
